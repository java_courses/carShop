package dp9v.carShop.main;

/**
 * Created by dpolkovnikov on 14.02.17.
 */

import dp9v.carShop.datamanager.DataManager;
import dp9v.carShop.models.*;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.*;

public class Store {
	private static final String FILE_CONTRACTS = "fileContracts.txt";
	private static final String FILE_CARS      = "fileCars.txt";
	private static final String FILE_CLIENTS   = "fileClients.txt";

	private static Logger logger = Logger.getLogger(Store.class);

	static {
		BasicConfigurator.configure();
	}

	private              HashMap<Order, Client> contractList   = new HashMap<>(256);
	private              HashSet<Car>           cars           = new HashSet<>(32);
	private              HashSet<Client>        clients        = new HashSet<>(256);

	public void createCar(int price, String model,
	                      String regNumber) {
		Car car = new Car(price, model, regNumber);
		cars.add(car);
	}

	public void save() {
		DataManager.serialize(cars, FILE_CARS);
		DataManager.serialize(clients, FILE_CLIENTS);
		DataManager.serialize(contractList, FILE_CONTRACTS);
	}

	public void recover() {
		ArrayList<Car> list = new ArrayList<>();
		DataManager.deserialize(FILE_CARS, list);
		for(Car car :
				list) {
			cars.add(car);
		}

		ArrayList<Client> listClient = new ArrayList<>();
		DataManager.deserialize(FILE_CLIENTS, listClient);
		for(Client client :
				listClient) {
			clients.add(client);
		}

		ArrayList<Order>  contractListOne = new ArrayList<>();
		ArrayList<Client> contractListTwo = new ArrayList<>();
		DataManager.deserialize(FILE_CONTRACTS, contractList);
	}

	public Order getFirstOrder() {
		for(Order order :
				contractList.keySet()) {
			return order;
		}
		return null;

	}

	public void sellCar(String model,
	                    String firstName,
	                    String lastName,
	                    String phoneNumber) throws CarNotFoundException {
		Client client = new Client(firstName,
		                           lastName, phoneNumber);
		clients.add(client);

		Car tmpCar = null;
		for(Car car :
				cars) {
			if(car.getModel().equals(model)) {
				tmpCar = car;
				break;
			}
		}
		if(tmpCar != null) {
			Random random = new Random();
			Order order = new Order(tmpCar,
			                        tmpCar.getPrice() * 2,
			                        random.nextLong(), (short) 80
			);
			contractList.put(order, client);
			cars.remove(tmpCar);
		} else {
			CarNotFoundException ex=new CarNotFoundException();
			logger.error("car with model "+model+" was not found", ex);
			throw ex;
		}
	}

	public ArrayList<Order> getOrders() {
		ArrayList<Order> list = new ArrayList<>(getOrders());
		return list;
	}

	public ArrayList<Car> getFreeCars() {
		ArrayList<Car> list = new ArrayList<>(cars);
		return list;
	}

	public Map<Order, Client> getContractList() {
		return contractList;
	}
}
