package dp9v.carShop.helpers;

/**
 * Created by dpolkovnikov on 15.02.17.
 */

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class Mailer {

	public static void sendMessage(String host, String port, String subject,
	                               String message, String mailTo) {
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.socketFactory.port", port);
		props.put("mail.smtp.socketFactory.class",
		          "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);


		Session session = Session.getDefaultInstance(props,
		                                      new javax.mail.Authenticator() {
			                                      protected PasswordAuthentication getPasswordAuthentication() {
				                                      return new PasswordAuthentication(Constants.EMAIL, Constants.PASS);
			                                      }
		                                      });
		try {

			Message message_ = new MimeMessage(session);
			message_.setFrom(new InternetAddress(Constants.EMAIL));
			message_.setRecipients(Message.RecipientType.TO,
			                      InternetAddress.parse(mailTo));
			message_.setSubject(subject);
			message_.setText(message);

			Transport.send(message_);
		}
		catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}
