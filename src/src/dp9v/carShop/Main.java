package dp9v.carShop;

import dp9v.carShop.main.CarNotFoundException;
import dp9v.carShop.main.Store;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * Created by dpolkovnikov on 14.02.17.
 */
public class Main {
	public static final Logger logger = Logger.getLogger(Main.class);

	static {
		DOMConfigurator.configure("src/src/dp9v/carShop/resources/log4j.xml");
	}

	public static void main(String[] args) throws CarNotFoundException {
		Store store = new Store();
		logger.info("store created");
		store.recover();
//		store.createCar(500000, "kia-rio",
//		                "B146AA");
//		store.createCar(123, "lada",
//		                "B125FA");
//		store.createCar(221112, "BMW",
//		                "B581PQ");
//		store.createCar(122221, "Delorian",
//		                "G124QS");
//
//		store.sellCar("Delorian","Hagrid", "Ded", "14-88-228" );
		logger.info(store);

       /* store.sellCar("kia-reva",
                "Jhon",
                "Konner" ,
                "+79126241898");
*/
//		store.save();
	}
}
