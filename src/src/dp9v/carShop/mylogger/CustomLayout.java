package dp9v.carShop.mylogger;

import org.apache.log4j.PatternLayout;
import org.apache.log4j.helpers.PatternConverter;
import org.apache.log4j.spi.LoggingEvent;

/**
 * Created by dpolkovnikov on 15.02.17.
 */
public class CustomLayout extends PatternLayout {
	private String subjectPattern = "%m-%n";
	private PatternConverter subject;
	private StringBuffer sbuf = new StringBuffer(256);

	public String getSubjectPattern() {
		return subjectPattern;
	}

	public void setSubjectPattern(String subjectPattern) {
		this.subjectPattern = subjectPattern;
		this.subject = this.createPatternParser(subjectPattern).parse();
	}

	public String formatSubject(LoggingEvent event) {
		if(this.sbuf.capacity() > 1024) {
			this.sbuf = new StringBuffer(256);
		} else {
			this.sbuf.setLength(0);
		}
		for(PatternConverter c = this.subject; c != null; c = c.next) {
			c.format(this.sbuf, event);
		}
		return this.sbuf.toString();
	}
}
