package dp9v.carShop.mylogger;

import dp9v.carShop.helpers.Mailer;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.spi.LoggingEvent;

/**
 * Created by dpolkovnikov on 15.02.17.
 */
public class CustomAppender extends AppenderSkeleton {
	private String mailTo;
	private String smtpHost;
	private String port;

	public String getMailTo() {
		return mailTo;
	}

	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	@Override
	protected void append(LoggingEvent loggingEvent) {
		String body    = layout.format(loggingEvent);
		String subject = null;

		if(layout instanceof CustomLayout) {
			subject = ((CustomLayout) layout).formatSubject(loggingEvent);
		} else {
			subject = "CarShop:" + loggingEvent.getLevel();
		}
		Mailer.sendMessage(getSmtpHost(), getPort(), subject, body, getMailTo());
	}

	@Override
	public boolean requiresLayout() {
		return false;
	}

	@Override
	public void close() {
	}
}
