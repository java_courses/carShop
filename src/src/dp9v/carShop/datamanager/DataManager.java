package dp9v.carShop.datamanager;

import dp9v.carShop.models.*;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.*;

/**
 * Created by dpolkovnikov on 14.02.17.
 */
public class DataManager {
	private static final Logger logger = Logger.getLogger(DataManager.class);

	static {
		BasicConfigurator.configure();
	}

	public static void serialize(Collection<? extends Serializable> list, String fileName) {
		try (FileOutputStream fos = new FileOutputStream(fileName);
		     ObjectOutputStream oos = new ObjectOutputStream(fos)) {
			for(Serializable serializable :
					list) {
				oos.writeObject(serializable);
			}

		}
		catch (FileNotFoundException e) {
			logger.error("File not found");
		}
		catch (IOException e) {
			logger.error("Can't read file");
		}
	}

	public static void serialize(Map<? extends Serializable,
			? extends Serializable> map, String fileName) {

		try (FileOutputStream fos = new FileOutputStream(fileName);
		     ObjectOutputStream oos = new ObjectOutputStream(fos)) {
			oos.writeObject(map);

		}
		catch (FileNotFoundException e) {
			logger.error("File not found");
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void deserialize(String fileName,
	                               HashMap<Order, Client> collection) {

		try (FileInputStream fis = new FileInputStream(fileName);
		     ObjectInputStream ois = new ObjectInputStream(fis)) {
			HashMap<Order, Client> collection2 = (HashMap<Order, Client>) ois.readObject();

			collection = (HashMap<Order, Client>)collection2.clone();

		}
		catch (FileNotFoundException e) {
			logger.error("File not found");
		}
		catch (IOException e) {
			logger.error("Can't read file");
		}
		catch (ClassNotFoundException e) {
			logger.error("Class not found");
		}
	}

	public static void deserialize(String fileName,
	                               List<Car> collection) {

		try (FileInputStream fis = new FileInputStream(fileName);
		     ObjectInputStream ois = new ObjectInputStream(fis)) {
			Car serializable;
			while((serializable = (Car) ois.readObject()) != null) {
				collection.add(serializable);
			}

		}
		catch (FileNotFoundException e) {
			logger.error("File not found");
		}
		catch (IOException e) {
			logger.error("Can't read file");
		}
		catch (ClassNotFoundException e) {
			logger.error("Class not found");
		}
	}

	public static void deserialize(String fileName,
	                               Collection<Client> collection) {

		try (FileInputStream fis = new FileInputStream(fileName);
		     ObjectInputStream ois = new ObjectInputStream(fis)) {
			Client serializable;
			while((serializable = (Client) ois.readObject()) != null) {
				collection.add(serializable);
			}

		}
		catch (FileNotFoundException e) {
			logger.error("File not found");
		}
		catch (IOException e) {
			logger.error("Can't read file");
		}
		catch (ClassNotFoundException e) {
			logger.error("Class not found");
		}
	}
}
